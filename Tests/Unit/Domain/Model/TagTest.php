<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 * @author Erwan Cadoret <erwan.cadobts@gmail.com>
 * @author Clément GILLERON <cl.gilleron@gmail.com>
 * @author Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 * @author Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 */
class TagTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getColorReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getColor()
        );
    }

    /**
     * @test
     */
    public function setColorForIntSetsColor()
    {
        $this->subject->setColor(12);

        self::assertAttributeEquals(
            12,
            'color',
            $this->subject
        );
    }
}
