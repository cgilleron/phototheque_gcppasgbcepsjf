<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 * @author Erwan Cadoret <erwan.cadobts@gmail.com>
 * @author Clément GILLERON <cl.gilleron@gmail.com>
 * @author Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 * @author Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 */
class AlbumTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Album
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Album();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDesriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDesription()
        );
    }

    /**
     * @test
     */
    public function setDesriptionForStringSetsDesription()
    {
        $this->subject->setDesription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'desription',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getShootingDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getShootingDate()
        );
    }

    /**
     * @test
     */
    public function setShootingDateForDateTimeSetsShootingDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setShootingDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'shootingDate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getThumbnailReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getThumbnail()
        );
    }

    /**
     * @test
     */
    public function setThumbnailForFileReferenceSetsThumbnail()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setThumbnail($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'thumbnail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPhotosReturnsInitialValueForPhoto()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPhotos()
        );
    }

    /**
     * @test
     */
    public function setPhotosForObjectStorageContainingPhotoSetsPhotos()
    {
        $photo = new \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Photo();
        $objectStorageHoldingExactlyOnePhotos = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePhotos->attach($photo);
        $this->subject->setPhotos($objectStorageHoldingExactlyOnePhotos);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOnePhotos,
            'photos',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addPhotoToObjectStorageHoldingPhotos()
    {
        $photo = new \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Photo();
        $photosObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $photosObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($photo));
        $this->inject($this->subject, 'photos', $photosObjectStorageMock);

        $this->subject->addPhoto($photo);
    }

    /**
     * @test
     */
    public function removePhotoFromObjectStorageHoldingPhotos()
    {
        $photo = new \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Photo();
        $photosObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $photosObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($photo));
        $this->inject($this->subject, 'photos', $photosObjectStorageMock);

        $this->subject->removePhoto($photo);
    }
}
