<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 * @author Erwan Cadoret <erwan.cadobts@gmail.com>
 * @author Clément GILLERON <cl.gilleron@gmail.com>
 * @author Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 * @author Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 */
class PhotoControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Controller\PhotoController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Controller\PhotoController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllPhotosFromRepositoryAndAssignsThemToView()
    {

        $allPhotos = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $photoRepository = $this->getMockBuilder(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\PhotoRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $photoRepository->expects(self::once())->method('findAll')->will(self::returnValue($allPhotos));
        $this->inject($this->subject, 'photoRepository', $photoRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('photos', $allPhotos);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenPhotoToView()
    {
        $photo = new \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Photo();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('photo', $photo);

        $this->subject->showAction($photo);
    }
}
