<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 * @author Erwan Cadoret <erwan.cadobts@gmail.com>
 * @author Clément GILLERON <cl.gilleron@gmail.com>
 * @author Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 * @author Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 */
class TagControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Controller\TagController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Controller\TagController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTagsFromRepositoryAndAssignsThemToView()
    {

        $allTags = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tagRepository = $this->getMockBuilder(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\TagRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $tagRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTags));
        $this->inject($this->subject, 'tagRepository', $tagRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('tags', $allTags);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenTagToView()
    {
        $tag = new \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('tag', $tag);

        $this->subject->showAction($tag);
    }
}
