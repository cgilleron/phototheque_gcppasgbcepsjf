<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'PhotothequeGCPPASGBCEPSJF.PhotothequeGcppasgbcepsjf',
            'Pi1',
            [
                'Photo' => 'list, show, search, cartography',
                'Comment' => 'list, new, create'
            ],
            // non-cacheable actions
            [
                'Photo' => 'search',
                'Comment' => 'create'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'PhotothequeGCPPASGBCEPSJF.PhotothequeGcppasgbcepsjf',
            'Pi2',
            [
                'Album' => 'list, show, search'
            ],
            // non-cacheable actions
            [
                'Album' => 'search'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'PhotothequeGCPPASGBCEPSJF.PhotothequeGcppasgbcepsjf',
            'Pi3',
            [
                'Tag' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Photo' => '',
                'Album' => '',
                'Tag' => '',
                'Comment' => 'create'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        pi1 {
                            iconIdentifier = phototheque_gcppasgbcepsjf-plugin-pi1
                            title = LLL:EXT:phototheque_gcppasgbcepsjf/Resources/Private/Language/locallang_db.xlf:tx_phototheque_gcppasgbcepsjf_pi1.name
                            description = LLL:EXT:phototheque_gcppasgbcepsjf/Resources/Private/Language/locallang_db.xlf:tx_phototheque_gcppasgbcepsjf_pi1.description
                            tt_content_defValues {
                                CType = list
                                list_type = photothequegcppasgbcepsjf_pi1
                            }
                        }
                        pi2 {
                            iconIdentifier = phototheque_gcppasgbcepsjf-plugin-pi2
                            title = LLL:EXT:phototheque_gcppasgbcepsjf/Resources/Private/Language/locallang_db.xlf:tx_phototheque_gcppasgbcepsjf_pi2.name
                            description = LLL:EXT:phototheque_gcppasgbcepsjf/Resources/Private/Language/locallang_db.xlf:tx_phototheque_gcppasgbcepsjf_pi2.description
                            tt_content_defValues {
                                CType = list
                                list_type = photothequegcppasgbcepsjf_pi2
                            }
                        }
                        pi3 {
                            iconIdentifier = phototheque_gcppasgbcepsjf-plugin-pi3
                            title = LLL:EXT:phototheque_gcppasgbcepsjf/Resources/Private/Language/locallang_db.xlf:tx_phototheque_gcppasgbcepsjf_pi3.name
                            description = LLL:EXT:phototheque_gcppasgbcepsjf/Resources/Private/Language/locallang_db.xlf:tx_phototheque_gcppasgbcepsjf_pi3.description
                            tt_content_defValues {
                                CType = list
                                list_type = photothequegcppasgbcepsjf_pi3
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'phototheque_gcppasgbcepsjf-plugin-pi1',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:phototheque_gcppasgbcepsjf/Resources/Public/Icons/user_plugin_pi1.svg']
			);
		
			$iconRegistry->registerIcon(
				'phototheque_gcppasgbcepsjf-plugin-pi2',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:phototheque_gcppasgbcepsjf/Resources/Public/Icons/user_plugin_pi2.svg']
			);
		
			$iconRegistry->registerIcon(
				'phototheque_gcppasgbcepsjf-plugin-pi3',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:phototheque_gcppasgbcepsjf/Resources/Public/Icons/user_plugin_pi3.svg']
			);
		
    }
);
