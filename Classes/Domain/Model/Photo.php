<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model;


/***
 *
 * This file is part of the "PhotothèqueGCPPASGBCEPSJF" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 *           Erwan Cadoret <erwan.cadobts@gmail.com>
 *           Clément GILLERON <cl.gilleron@gmail.com>
 *           Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 *           Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 *
 ***/
/**
 * Photo
 */
class Photo extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * Titre
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * Description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * Fichier
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $image = null;

    /**
     * Date de prise de vue
     * 
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $shootingDate = null;

    /**
     * Auteur
     * 
     * @var string
     */
    protected $author = '';

    /**
     * Lieu
     * 
     * @var string
     */
    protected $place = '';

    /**
     * Sujets
     * 
     * @var string
     */
    protected $models = '';

    /**
     * Tags
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $tags = null;

    /**
     * Commentaires
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $comments = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the image
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the shootingDate
     * 
     * @return \DateTime $shootingDate
     */
    public function getShootingDate()
    {
        return $this->shootingDate;
    }

    /**
     * Sets the shootingDate
     * 
     * @param \DateTime $shootingDate
     * @return void
     */
    public function setShootingDate(\DateTime $shootingDate)
    {
        $this->shootingDate = $shootingDate;
    }

    /**
     * Returns the author
     * 
     * @return string $author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Sets the author
     * 
     * @param string $author
     * @return void
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Returns the place
     * 
     * @return string $place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Sets the place
     * 
     * @param string $place
     * @return void
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * Returns the models
     * 
     * @return string $models
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * Sets the models
     * 
     * @param string $models
     * @return void
     */
    public function setModels($models)
    {
        $this->models = $models;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->tags = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->comments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Tag
     * 
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag $tag
     * @return void
     */
    public function addTag(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag $tag)
    {
        $this->tags->attach($tag);
    }

    /**
     * Removes a Tag
     * 
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag $tagToRemove The Tag to be removed
     * @return void
     */
    public function removeTag(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag $tagToRemove)
    {
        $this->tags->detach($tagToRemove);
    }

    /**
     * Returns the tags
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag> $tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Sets the tags
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Tag> $tags
     * @return void
     */
    public function setTags(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $tags)
    {
        $this->tags = $tags;
    }

    /**
     * Adds a Comment
     * 
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment $comment
     * @return void
     */
    public function addComment(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment $comment)
    {
        $this->comments->attach($comment);
    }

    /**
     * Removes a Comment
     * 
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment $commentToRemove The Comment to be removed
     * @return void
     */
    public function removeComment(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment $commentToRemove)
    {
        $this->comments->detach($commentToRemove);
    }

    /**
     * Returns the comments
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment> $comments
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Sets the comments
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment> $comments
     * @return void
     */
    public function setComments(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $comments)
    {
        $this->comments = $comments;
    }
}
