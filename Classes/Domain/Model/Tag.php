<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model;


/***
 *
 * This file is part of the "PhotothèqueGCPPASGBCEPSJF" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 *           Erwan Cadoret <erwan.cadobts@gmail.com>
 *           Clément GILLERON <cl.gilleron@gmail.com>
 *           Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 *           Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 *
 ***/
/**
 * Tag
 */
class Tag extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * Titre
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * Couleur
     * 
     * @var int
     */
    protected $color = 0;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the color
     * 
     * @return int $color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Sets the color
     * 
     * @param int $color
     * @return void
     */
    public function setColor($color)
    {
        $this->color = $color;
    }
}
