<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Controller;


/***
 *
 * This file is part of the "PhotothèqueGCPPASGBCEPSJF" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 *           Erwan Cadoret <erwan.cadobts@gmail.com>
 *           Clément GILLERON <cl.gilleron@gmail.com>
 *           Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 *           Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 *
 ***/
/**
 * PhotoController
 */
class PhotoController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * photoRepository
     * 
     * @var \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\PhotoRepository
     */
    protected $photoRepository = null;

    /**
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\PhotoRepository $photoRepository
     */
    public function injectPhotoRepository(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\PhotoRepository $photoRepository)
    {
        $this->photoRepository = $photoRepository;
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $photos = $this->photoRepository->findAll();
        $this->view->assign('photos', $photos);
    }

    /**
     * action show
     * 
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Photo $photo
     * @return void
     */
    public function showAction(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Photo $photo)
    {
        $this->view->assign('photo', $photo);
    }

    /**
     * action search
     * 
     * @return void
     */
    public function searchAction()
    {
    }

    /**
     * action cartography
     * 
     * @return void
     */
    public function cartographyAction()
    {
    }
}
