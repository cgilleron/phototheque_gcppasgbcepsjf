<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Controller;


/***
 *
 * This file is part of the "PhotothèqueGCPPASGBCEPSJF" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 *           Erwan Cadoret <erwan.cadobts@gmail.com>
 *           Clément GILLERON <cl.gilleron@gmail.com>
 *           Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 *           Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 *
 ***/
/**
 * AlbumController
 */
class AlbumController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * albumRepository
     * 
     * @var \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\AlbumRepository
     */
    protected $albumRepository = null;

    /**
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\AlbumRepository $albumRepository
     */
    public function injectAlbumRepository(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\AlbumRepository $albumRepository)
    {
        $this->albumRepository = $albumRepository;
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $albums = $this->albumRepository->findAll();
        $this->view->assign('albums', $albums);
    }

    /**
     * action show
     * 
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Album $album
     * @return void
     */
    public function showAction(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Album $album)
    {
        $this->view->assign('album', $album);
    }

    /**
     * action search
     * 
     * @return void
     */
    public function searchAction()
    {
    }
}
