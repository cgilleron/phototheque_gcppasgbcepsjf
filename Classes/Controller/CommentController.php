<?php
namespace PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Controller;


/***
 *
 * This file is part of the "PhotothèqueGCPPASGBCEPSJF" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Jean-François Picherit-Steinbrucker  <jfps.dev21@gmail.com>
 *           Erwan Cadoret <erwan.cadobts@gmail.com>
 *           Clément GILLERON <cl.gilleron@gmail.com>
 *           Bryan SEGUINEAUD-GANCINHO  <bryan.seguineaud-gancinho@etu.y-bordeaux.fr>
 *           Alexandre PERROT-POUSSET  <alexperrotpousset@gmail.com>
 *
 ***/
/**
 * CommentController
 */
class CommentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * commentRepository
     * 
     * @var \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\CommentRepository
     */
    protected $commentRepository = null;

    /**
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\CommentRepository $commentRepository
     */
    public function injectCommentRepository(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Repository\CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $comments = $this->commentRepository->findAll();
        $this->view->assign('comments', $comments);
    }

    /**
     * action new
     * 
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * action create
     * 
     * @param \PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment $newComment
     * @return void
     */
    public function createAction(\PhotothequeGCPPASGBCEPSJF\PhotothequeGcppasgbcepsjf\Domain\Model\Comment $newComment)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->commentRepository->add($newComment);
        $this->redirect('list');
    }
}
